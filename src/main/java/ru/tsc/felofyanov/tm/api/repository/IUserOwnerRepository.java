package ru.tsc.felofyanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    void clear(@NotNull String userId);

    @Nullable
    M add(@Nullable String userId, @NotNull M model);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    M removeByIndex(@NotNull String userId, @NotNull Integer index);
}
